package com.usereserva.api.service.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.usereserva.api.entity.Person;
import com.usereserva.api.entity.inputs.PersonInput;
import com.usereserva.api.service.PersonService;

@Component
public class PersonQuery implements GraphQLQueryResolver{
	
	@Autowired
	private PersonService service;
	
    public List<Person> getPersons(final int count, PersonInput input) {
    	return service.getPersons(count);
    }
    
    public Optional<Person> getPerson(final Long id) {
        return service.getPerson(id);
    }

}
